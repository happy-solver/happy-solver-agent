package happysolveragent.service;

import static java.util.Arrays.asList;

import java.util.List;

import happysolveragent.rest.resources.BinPackingItem;
import happysolveragent.rest.resources.BinPackingModel;

public class TestModelBuilder {

	static BinPackingModel buildModelRest() {

		BinPackingItem itemRest = new BinPackingItem();
		itemRest.setAmount(3);
		itemRest.setName("1");

		BinPackingItem itemRest2 = new BinPackingItem();
		itemRest2.setAmount(3);
		itemRest2.setName("2");

		List<BinPackingItem> items = asList(itemRest, itemRest2);

		BinPackingModel modelRest = new BinPackingModel();
		modelRest.setName("MyFirstModel");
		modelRest.setId(386L);
		modelRest.setCapacity(10);
		modelRest.setItems(items);

		return modelRest;
	}

}
