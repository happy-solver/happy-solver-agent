package happysolveragent.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.RabbitMQContainer;

import happysolveragent.rest.resources.BinPackingModel;
import happysolveragent.rest.resources.BinPackingSolution;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RabbitMQSchedulerTest {

	@ClassRule
	public static RabbitMQContainer rabbitMQContainer = new RabbitMQContainer("rabbitmq:3.7-management")
			.withExposedPorts(5672).withVhost("/");

	@BeforeClass
	public static void setupClass() {
		rabbitMQContainer.start();

		String propertyHost = "spring.rabbitmq.host";
		String host = rabbitMQContainer.getContainerIpAddress();
		System.setProperty(propertyHost, host);

		String propertyPort = "spring.rabbitmq.port";
		String port = rabbitMQContainer.getFirstMappedPort() + "";
		System.setProperty(propertyPort, port);
	}

	@Autowired
	private RabbitMQClient client;

	@Test
	public void send_model_receive_solution() throws Exception {

		BinPackingModel model = TestModelBuilder.buildModelRest();
		client.sendModel(model);

		BinPackingSolution receiveSolution = client.receiveSolution();
		int counter = 0;
		while (receiveSolution == null && counter < 10) {
			receiveSolution = client.receiveSolution();
			Thread.sleep(1_000);
			counter++;
		}

		assertThat(receiveSolution).isNotNull();
		assertThat(receiveSolution.getModelId()).isEqualTo(model.getId());
		assertThat(receiveSolution.getModelName()).isEqualTo(model.getName());
	}
}
