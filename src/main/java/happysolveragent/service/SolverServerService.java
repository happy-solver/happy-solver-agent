package happysolveragent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import happysolveragent.config.AgentConfiguration;
import happysolveragent.rest.SolverServerClient;
import happysolveragent.rest.resources.AgentRegister;
import lombok.extern.flogger.Flogger;

@Flogger
@Service
public class SolverServerService {

	@Autowired
	private SolverServerClient solverServerClient;

	@Autowired
	private AgentConfiguration configuration;

	public boolean registerAgent() {
		AgentRegister agentRegister = new AgentRegister(configuration.getAgentName(),
				configuration.getAgentStartInstant());
		try {
			solverServerClient.register(agentRegister);
			return true;
		} catch (Exception e) {
			log.atFine().withCause(e).log("Error to register agent.");
			return false;
		}
	}
}
