package happysolveragent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import happysolveragent.rest.resources.BinPackingModel;
import happysolveragent.rest.resources.BinPackingSolution;
import lombok.extern.flogger.Flogger;

@Flogger
@Service
public class BinPackingService {

	@Autowired
	private BinPackingSolverService solverService;

	@Autowired
	private RabbitMQClient rabbitMQClient;

	private boolean isOptimizationRunning = false;

	public void runSolverSendSolution(BinPackingModel model) {
		if (isOptimizationRunning) {
			log.atWarning().log("Optimization is already running. Skip recieve model from RabbitMQ.");
			return;
		}
		isOptimizationRunning = true;
		BinPackingSolution solution = solverService.runSolver(model);
		isOptimizationRunning = false;
		rabbitMQClient.sendSolution(solution);
		log.atInfo().log("Send soltuion.");
	}

	public boolean isOptimizationRunning() {
		return isOptimizationRunning;
	}

	public void setOptimizationRunning(boolean isOptimizationRunning) {
		this.isOptimizationRunning = isOptimizationRunning;
	}
}