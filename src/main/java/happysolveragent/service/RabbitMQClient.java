package happysolveragent.service;

import static org.apache.commons.lang3.StringUtils.isBlank;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import happysolveragent.config.RabbitMQConfiguration;
import happysolveragent.rest.resources.BinPackingModel;
import happysolveragent.rest.resources.BinPackingSolution;

@Component
public class RabbitMQClient {

	@Autowired
	private RabbitMQConfiguration configuration;

	@Autowired
	private RabbitTemplate template;

	public void sendSolution(BinPackingSolution solution) {
		String solutionJson = JsonSerializer.toJson(solution, false);
		template.convertAndSend(configuration.getSolutionQueue(), solutionJson);
	}

	public void sendModel(BinPackingModel model) {
		sendJson(configuration.getModelQueue(), model);
	}

	private void sendJson(String queueName, Object toSend) {
		String json = JsonSerializer.toJson(toSend, false);
		template.convertAndSend(queueName, json);
	}

	public BinPackingModel receiveModel() {
		String modelString = (String) template.receiveAndConvert(configuration.getModelQueue());
		if (isBlank(modelString)) {
			return null;
		}
		return JsonSerializer.fromJson(modelString, BinPackingModel.class);
	}

	public BinPackingSolution receiveSolution() {
		String solutionString = (String) template.receiveAndConvert(configuration.getSolutionQueue());
		if (isBlank(solutionString)) {
			return null;
		}
		return JsonSerializer.fromJson(solutionString, BinPackingSolution.class);
	}
}
