package happysolveragent.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import happysolveragent.rest.resources.BinPackingModel;
import happysolveragent.service.BinPackingService;
import happysolveragent.service.RabbitMQClient;
import lombok.extern.flogger.Flogger;

@Flogger
@Component
public class RabbitMQScheduler {

	@Autowired
	private RabbitMQClient rabbitService;

	@Autowired
	private BinPackingService binPackingService;

	@Scheduled(fixedDelayString = "${scheduler.model-queue-interval-millis}")
	public void run() {
		if (binPackingService.isOptimizationRunning()) {
			log.atFine().log("Optimization is already running. Skip recieve model from RabbitMQ.");
			return;
		}
		BinPackingModel model = rabbitService.receiveModel();
		if (model != null) {
			binPackingService.runSolverSendSolution(model);
		}
	}
}
