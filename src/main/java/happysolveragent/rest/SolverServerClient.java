package happysolveragent.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import happysolveragent.rest.resources.AgentRegister;

@FeignClient(value = SolverServerClient.MAPPING, fallbackFactory = SolverServerClientFallbackFactory.class)
public interface SolverServerClient {

	String MAPPING = "solver-server";
	String REGISTER = "/agents/register";

	@PostMapping(REGISTER)
	void register(@RequestBody AgentRegister register);
}
