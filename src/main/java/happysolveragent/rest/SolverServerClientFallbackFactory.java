package happysolveragent.rest;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;
import lombok.extern.flogger.Flogger;

@Flogger
@Component
public class SolverServerClientFallbackFactory implements FallbackFactory<SolverServerClient> {
	@Override
	public SolverServerClient create(Throwable cause) {
		log.atWarning().log("Unexpected connection problem " + cause.getMessage());
		log.atFine().withCause(cause);
		return new SolverServerClientFallback(cause);
	}
}
