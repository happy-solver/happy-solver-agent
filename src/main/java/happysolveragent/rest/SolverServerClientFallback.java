package happysolveragent.rest;

import happysolveragent.rest.resources.AgentRegister;
import lombok.Data;

@Data
public class SolverServerClientFallback implements SolverServerClient {

	private final Throwable cause;

	@Override
	public void register(AgentRegister register) {

	}
}
