package happysolveragent.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class RabbitMQConfiguration {

	@Value("${spring.rabbitmq.model-queue}")
	private String modelQueue;

	@Value("${spring.rabbitmq.solution-queue}")
	private String solutionQueue;

	@Bean
	public Queue modelQueue() {
		return new Queue(modelQueue);
	}

	@Bean
	public Queue solutionQueue() {
		return new Queue(solutionQueue);
	}
}
