package happysolveragent.controller;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import happysolveragent.rest.resources.BinPackingModel;
import happysolveragent.service.BinPackingService;
import lombok.extern.flogger.Flogger;

@Flogger
@RestController
@RequestMapping(BinPackingController.MAPPING)
public class BinPackingController {

	public static final String MAPPING = "/binpacking";
	public static final String RUN_SOLVER = "/runSolver";

	@Autowired
	@Qualifier("rest-thread-pool") // own pool for graceful shutdown
	private ThreadPoolTaskScheduler threadPool;

	@Autowired
	private BinPackingService binPackingService;

	@PostMapping(RUN_SOLVER)
	public ResponseEntity<String> runSolver(@RequestBody BinPackingModel model) {

		String msg = "Run solver on model: " + model.getName();
		log.atInfo().log(msg);
		CompletableFuture.runAsync(() -> binPackingService.runSolverSendSolution(model), threadPool);
		return ResponseEntity.ok(msg);
	}
}
